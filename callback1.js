const fs = require('fs');

module.exports = function callback(boardsDataPath, boardId, cb) {

    setTimeout(() => {

        if (typeof cb !== 'function') {
            console.error(new Error("Cb function is not passed"));
        } else {

            fs.readFile(boardsDataPath, 'utf8', (err, fileData) => {
                if (err) {
                    // console.error(err);
                    cb("Path Error: " + err.message);
                }
                else {
                    try {

                        let boardsData = JSON.parse(fileData);
                        if (Array.isArray(boardsData)) {

                            let board = boardsData.find((boardData) => {
                                return boardData.id == boardId;
                            });

                            if (board != undefined) {
                                cb(null, board);
                            }
                            else {
                                cb(new Error("Board data not found for " + boardId));
                            }
                        }
                        else {
                            cb(new Error("Data not in a correct format"));
                        }
                    } catch (err) {
                        cb("JSON FILE ERROR: " + err.message);
                    }


                }
            });
        }
    }, 2000);
}