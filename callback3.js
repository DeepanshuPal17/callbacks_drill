const fs = require('fs');

module.exports = function callback(cardsDataPath, listId, cb) {
    setTimeout(() => {
        if (typeof cb === 'function') {

            fs.readFile(cardsDataPath, 'utf8', (err, fileData) => {
                if (err) {
                    console.error("Path Error: " + err.message);
                }
                else {

                    try {
                        let cardsData = JSON.parse(fileData);
                        if (cardsData[listId] != undefined)
                            cb(null, cardsData[listId]);
                        else {
                            cb(new Error("Card data not found for " + listId));
                        }
                    }
                    catch (err) {
                        cb("JSON FILE ERROR: " + err.message);
                    }
                }
            });
        }
        else {
            console.error(new Error("Cb function is not passed"));
        }
    }, 2000)
}