const fs = require('fs');

module.exports = function callback(listDataPath, boardId, cb) {
    setTimeout(() => {
        if (typeof cb === 'function') {

            fs.readFile(listDataPath, 'utf8', (err, fileData) => {
                if (err) {
                    console.error("Path Error: " + err.message);
                }
                else {

                    try {
                        let listData = JSON.parse(fileData);
                        if (listData[boardId] != undefined) {
                            cb(null, listData[boardId]);
                        }
                        else {
                            cb(new Error("List data not Exist for " + boardId));
                        }
                    }
                    catch (err) {
                        cb("JSON FILE ERROR: " + err.message);
                    }
                }
            });
        }
        else {
            console.error(new Error("Cb function is not passed"));
        }
    }, 2000);
}