const callback3 = require('../callback3');
const path = require('path');
const cardsDataPath = path.join(__dirname, '../cards.json');

function cb(err, card) {
    if (err) {
        console.error(err);
    }
    else
        console.log(card);
}
callback3(cardsDataPath, "qwsa221", cb);