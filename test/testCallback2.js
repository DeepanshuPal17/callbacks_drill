const callback2 = require('../callback2');
const path = require('path');
const listDataPath = path.join(__dirname, '../lists.json');

function cb(err, list) {
    if (err) {
        console.error(err);
    }
    else {
        console.log(list);
    }
}
callback2(listDataPath, "mcu453ed", cb);