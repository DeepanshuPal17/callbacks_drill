const fs = require('fs');
const lists = require('./callback2');
const cards = require('./callback3');
const path = require('path');
const boadrsDataPath = path.join(__dirname, './boards.json');
const listDataPath = path.join(__dirname, './lists.json');
const cardsDataPath = path.join(__dirname, './cards.json');

module.exports = function callback(name, cb) {
    if (typeof cb == 'function') {

        setTimeout(() => {
            fs.readFile(boadrsDataPath, 'utf8', (err, fileData) => {
                if (err) {
                    cb("Path ERROR: " + err.message);
                }
                else {
                    try {
                        // let data = [];
                        let boardsData = JSON.parse(fileData);
                        let boardId = boardsData.find((boardData) => {
                            if (boardData.name == name) {
                                return true;
                            }
                        });
                        if (boardId == undefined) {
                            cb(new Error("Board Data Not Found "));
                        }
                        else {

                            cb(null, boardId);
                            lists(listDataPath, boardId.id, (err, listData) => {
                                if (err) {

                                    cb(err);
                                }
                                else {
                                    let listIdMind = listData.find((list) => {
                                        if (list.name == "Mind") {
                                            return true;
                                        }
                                    });
                                    let listIdSpace = listData.find((list) => {
                                        if (list.name == "Space") {
                                            return true;
                                        }
                                    });

                                    cb(null, listIdMind);
                                    cb(null, listIdSpace)
                                    cards(cardsDataPath, listIdMind.id, (err, card) => {
                                        if (err) {
                                            cb(err);
                                        }
                                        else {

                                            cb(null, card);

                                        }
                                    });
                                    cards(cardsDataPath, listIdSpace.id, (err, card) => {
                                        if (err) {
                                            cb(err);
                                        }
                                        else {

                                            cb(null, card);
                                        }
                                    });

                                }
                            });
                        }
                    }
                    catch (err) {
                        console.error(err);
                    }
                }
            });
        }, 2000);
    }
    else {
        console.error(new Error("Cb is not a function"));
    }
}